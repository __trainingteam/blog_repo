from django.http import HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404

from .models import TextPost


def index(request):
    post_list = TextPost.objects.order_by('-pub_date')[:20]
    template = loader.get_template("blog/index.html")
    context = {"post_list": post_list}
    return HttpResponse(template.render(context, request))


def detail(request, post_id):
    post = get_object_or_404(TextPost, pk=post_id)
    template = loader.get_template("blog/detail.html")
    context = {"post": post}
    return HttpResponse(template.render(context, request))
